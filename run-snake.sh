#!/bin/bash
rsync singlecell-pipeline/Snakefile.from_fastq ./Snakefile
rsync singlecell-pipeline/Snakefile.utils .
rsync singlecell-pipeline/Snakefile.qc .
#rsync singlecell-pipeline/run_filter_reads.py .
#rsync singlecell-pipeline/get_paired.py .
#rsync singlecell-pipeline/count_skips.py .
rsync singlecell-pipeline/make_df.sh .
snakemake --cluster "sbatch --job-name={params.name} --ntasks=1 --cpus-per-task={threads} \
           --partition={params.partition} --mem={params.mem} -o slurm_output/{params.name}-%j" \
            -p -T -k -j 100 -w 100 --rerun-incomplete -R 
