# function to count junction hits
# each mapped junction is recorded by its gene name, chromosome, and start and end positions
import sys
sys.path.append("/local10G/resources/py2.7.9_virtualenv/lib/python2.7/site-packages/")
import HTSeq as ht
import pandas as pd
from collections import Counter

input_bam=sys.argv[1]
output_prefix=sys.argv[2]
gtf_reference=sys.argv[3]

# perform cut -d';' -f 1 <ref.gtf> | awk '($3 ~ /exon/)' | uniq 
# before loading reference so that every (correctly ordered) exon appears
# only once

# If the gtf file has genes with >1 name or overlapping genes, reads
# that map to those may not be counted

i=0;
MIN_EXON_SEP=1

exon_num={}
prev_end={}
exons=ht.GenomicArrayOfSets("auto",stranded=False)
gtf_file=ht.GFF_Reader(gtf_reference)
for feature in gtf_file:
    if feature.type == "exon":
        exon_num[feature.name]=0
        prev_end[feature.name]=0
    
for feature in gtf_file:
     # initialize prev_name and end
    if feature.type == "exon":

        if (feature.iv.start - prev_end[feature.name]) > MIN_EXON_SEP: 
            exon_num[ feature.name ]+=1
        
        exon_name = feature.name + "_e" + format(exon_num[feature.name],'02')
        prev_end[feature.name]=feature.iv.end
        exons[ feature.iv ] += exon_name


in_reads=ht.BAM_Reader(input_bam)
junction_counts = Counter([])
for read in in_reads:
  matched_exons=list([]); i=0
  iv_seq = ( a.ref_iv for a in read.cigar if a.type == "M" and a.size > 0 )
  for iv in iv_seq:
    for iv2, fs2 in exons[ iv ].steps():
      matched_exons.append(set([exon[:-4] for exon in fs2]))
      i+=1
  
  gene_names = set.intersection(*matched_exons) 
  if gene_names: # if read aligns to multiple non-overlapping genes, it will not be counted
    # only gene(s) to which all parts of read map is recorded
    gene_name = list(gene_names)[0]

    for a in read.cigar:
        if a.type == "N":
          junction_name = gene_name + ",," + str(a.ref_iv.start)+ "_" + a.ref_iv.chrom  + ",," + str(a.ref_iv.end)+ "_" + a.ref_iv.chrom
          junction_counts[junction_name]+=1

# save output as a tab-delimited table
count_tuple = [tuple(jun.split(",,")+ [junction_counts[jun]]) for jun in junction_counts]
count_pd = pd.DataFrame(count_tuple, columns=['gene','start','end','count'], index=count_tuple)
count_pd.to_csv(output_prefix+".jun_counts.tsv", sep="\t", header=False,index=False)

