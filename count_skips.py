import sys
sys.path.append("/local10G/resources/py2.7.9_virtualenv/lib/python2.7/site-packages/")
import HTSeq as ht
import pandas as pd

input_bam=sys.argv[1]
output_prefix=sys.argv[2]
gtf_reference=sys.argv[3]


# In[25]:

# perform cut -d';' -f 1 <ref.gtf> | awk '($3 ~ /exon/)' | uniq 
# before loading reference so that every (correctly ordered) exon appears
# only once


# If the gtf file has genes with >1 name or overlapping genes, reads
# that map to those may not be counted
# Sequentially name non-contiguous exons for each gene
# feature.name defaults to gene_id

i=0;
MIN_EXON_SEP=1

counts = {}
skip_counts = {}

exon_num={}
prev_end={}
exons=ht.GenomicArrayOfSets("auto",stranded=False)
gtf_file=ht.GFF_Reader(gtf_reference)
for feature in gtf_file:
    if feature.type == "exon":
        exon_num[feature.name]=0
        prev_end[feature.name]=0
    
for feature in gtf_file:
     # initialize prev_name and end
    if feature.type == "exon":

        if (feature.iv.start - prev_end[feature.name]) > MIN_EXON_SEP: 
            exon_num[ feature.name ]+=1
        
        exon_name = feature.name + "_e" + format(exon_num[feature.name],'02')
        prev_end[feature.name]=feature.iv.end
        exons[ feature.iv ] += exon_name
        counts[exon_name]=0
        skip_counts[exon_name]=0

# function to count reads and junctions that skip an exon
in_reads=ht.BAM_Reader(input_bam)
for read in in_reads:
    matched_exons=set([])
    iv_seq = ( co.ref_iv for co in read.cigar if co.type == "M" and co.size > 0 )
    for iv in iv_seq:
      for iv2, fs2 in exons[ iv ].steps():
        matched_exons |= fs2
    for exon in matched_exons:
        counts[exon]+=1

    matched_gene=list(matched_exons)[0][:-4]
    for a in read.cigar:
        if a.type == "N":
            for iv, val in exons[a.ref_iv].steps():
                for exon in val:
                    if exon not in matched_exons and exon[:-4]==matched_gene:
                        skip_counts[exon]+=1

# save output as a tab-delimited table
genes=pd.Series(data=[gene[:-4] for gene in counts.keys()], index=counts.keys())
count_pd = pd.DataFrame({ "gene":pd.Series(genes),"counts" : pd.Series(counts), "skip_counts" : pd.Series(skip_counts)})
count_pd.to_csv(output_prefix+".counts.tsv", sep="\t", header=False)

