rm(list=ls())
require(data.table)
require(ggplot2)
require(reshape2)
require(RColorBrewer)
require(plyr)
require(doMC)
registerDoMC(6)
source("~/singlecell-pipeline/rnaseq_funcs_V2.R")
setwd('~/singlecell-pipeline/fetal_venule/exp1_nextseq/')

load('data/fv1_NS.dt_goodCells.RData')
load('data/fetal_venule_exp1_Nextseq.cast_goodCells_log10cpm.RData')


# munge -----
# remove genes not detected in dataset
dt.fv1_NS = fread('data/fetal_venule_exp1_Nextseq.htseq_count.df')
setkey(dt.fv1_NS);
dt.fv1_NS[,cell.name := paste0('fv1_NS_', V1)]
dt.fv1_NS = dt.fv1_NS[, .(cell.name, V2, V3)]
setnames(dt.fv1_NS, c('cell.name','gene','counts'))

cast.fv1_NS = as.matrix(cast.to.df(dt.fv1_NS, expression.values = 'counts'))

numcells = colSums(cast.fv1_NS > 0)
genes.Detected = names(numcells[numcells > 0])
cast.fv1_NS.noTechnical = cast.fv1_NS[,genes.Detected]

htseq = colnames(cast.fv1_NS.noTechnical)[colnames(cast.fv1_NS.noTechnical) %like% "__"]
htseq.unaligned = cast.fv1_NS.noTechnical[,htseq]
save(htseq.unaligned, file = 'data/fv1_NS.htseq_unaligned.RData')

ercc = colnames(cast.fv1_NS.noTechnical)[colnames(cast.fv1_NS.noTechnical) %like% "ERCC.0"]
ercc.reads = cast.fv1_NS.noTechnical[,ercc]
save(ercc.reads, file = 'data/fv1_NS.ercc_reads.RData')

ncol(cast.fv1_NS.noTechnical)
cast.fv1_NS.noTechnical = cast.fv1_NS.noTechnical[, !colnames(cast.fv1_NS.noTechnical) %in% c(htseq, ercc)]
ncol(cast.fv1_NS.noTechnical)

# normalize to counts per million
numcounts = rowSums(cast.fv1_NS.noTechnical)
cast.fv1_NS.cpm = 1e6 * cast.fv1_NS.noTechnical / numcounts
summary(rowSums(cast.fv1_NS.cpm)) # every cell should have rowSum = 1e6

cast.fv1_NS.log10.cpm = log10(cast.fv1_NS.cpm + 1)

hist(cast.fv1_NS.log10.cpm[, 'Mki67'])

rm(cast.fv1_NS.cpm)

# add experiment and qc info -----
# make sure matrices are in same order
ercc.reads = ercc.reads[ order(rownames(ercc.reads)), ]
ercc.summary = data.table(cell.name = rownames(ercc.reads), ercc.sum = rowSums(ercc.reads))
htseq.unaligned = htseq.unaligned[ order(rownames(htseq.unaligned)), ]
cast.fv1_NS.noTechnical = cast.fv1_NS.noTechnical[ order(rownames(cast.fv1_NS.noTechnical)), ]

fv1_NS.cell.info = data.frame(cell.name = rownames(cast.fv1_NS.noTechnical), num.genes = rowSums(cast.fv1_NS.noTechnical > 0),
                       num.mouse.reads = rowSums(cast.fv1_NS.noTechnical), num.rRNA.reads = cast.fv1_NS.noTechnical[,'Rn45s'])
fv1_NS.cell.info = as.data.table(cbind(fv1_NS.cell.info, htseq.unaligned), keep.rownames = F)
setkey(fv1_NS.cell.info)

fv1_NS.cell.info = merge(fv1_NS.cell.info, ercc.summary, by = 'cell.name')

expt <- paste0('p',1:10); col <- brewer.pal(length(expt),'Spectral')
expt.col <- data.table(cbind(expt, col))
setnames(expt.col,c("sort.plate","sort.plate_color")); 
expt2col <- expt.col[['sort.plate_color']]; names(expt2col) <- expt.col[['sort.plate']]

fv1_NS.cell.info[, sort.plate := substr(cell.name, 8, regexpr('c[0-9]', cell.name) - 1)]
setkey(fv1_NS.cell.info)
fv1_NS.cell.info = merge(fv1_NS.cell.info, expt.col, by = 'sort.plate')

# add dissection info
fv1_NS.cell.info[,dissection := 'SV']; 
fv1_NS.cell.info[sort.plate %in% paste0('p',6:10),dissection:='CV']
fv1_NS.cell.info[,dissection_color := gg_color_hue(2)[2]]
fv1_NS.cell.info[dissection == 'CV',dissection_color := gg_color_hue(2)[1]]
fv1_NS.cell.info[,dissection := factor(dissection, levels = c('CV','SV'))]

# qc plots ------
ggplot(fv1_NS.cell.info, aes(log10(num.mouse.reads + 1), num.genes, color = sort.plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw() + geom_vline(xintercept = 5) + 
  xlab('mm10-aligned reads, log10') + ylab('Genes Detected, counts > 0') + 
  ggtitle('round 1 QC, FV1_NS')
ggsave('qc/fv1_NS.num.genes_numReads_all.pdf', height = 4, width = 6)

fv1_NS.cell.info[, htseq.sum := sum(`__alignment_not_unique`, `__ambiguous`, `__no_feature`, `__not_aligned`,
                                    `__too_low_aQual`)]

fv1_NS.cell.info[,frac.mRNA := 1 - (num.rRNA.reads + htseq.sum + ercc.sum)/(htseq.sum + ercc.sum + num.mouse.reads)]
fv1_NS.cell.info[,frac.rRNA := num.rRNA.reads/num.mouse.reads]
ggplot(fv1_NS.cell.info[num.mouse.reads > 1e5], aes(num.genes, frac.rRNA, color = sort.plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw() + ggtitle('Fraction mapping to Rn45s\nall aligned mouse reads') +
  geom_hline(yintercept = .4)
ggsave('qc/numGenes_fraction_rRNA_all.pdf', height = 5, width = 7)

# remove low-number of genes and cells 
cells.best = fv1_NS.cell.info[num.mouse.reads > 1e5][frac.rRNA < .4, cell.name]

cast.fv1_NS.good = cast.fv1_NS.log10.cpm[rownames(cast.fv1_NS.log10.cpm) %in% cells.best, ]
save(cast.fv1_NS.good, file='data/fetal_venule_exp1_Nextseq.cast_goodCells_log10cpm.RData')
rm(cast.fv1_NS.log10.cpm)

dt.fv1_NS.good = data.table(melt(as.matrix(cast.fv1_NS.good)))
setnames(dt.fv1_NS.good, c('cell.name','gene','log10.cpm')); setkey(dt.fv1_NS.good)
save(dt.fv1_NS.good, file = 'data/fetal_venule_exp1_Nextseq.dt_goodCells.RData')

save(fv1_NS.cell.info, file = 'data/fetal_venule_exp1_Nextseq.cell_info.RData')

# still keeping some not-so-good cells - turns out to have a real effect on rPCA
cells.decent = fv1_NS.cell.info[num.mouse.reads > 2e4][frac.rRNA < .6, cell.name]

cast.fv1_NS.decent = cast.fv1_NS.log10.cpm[rownames(cast.fv1_NS.log10.cpm) %in% cells.decent, ]
save(cast.fv1_NS.decent, file='data/fetal_venule_exp1_Nextseq.cast_decentCells_log10cpm.RData')
rm(cast.fv1_NS.log10.cpm)

dt.fv1_NS.decent = data.table(melt(as.matrix(cast.fv1_NS.decent)))
setnames(dt.fv1_NS.decent, c('cell.name','gene','log10.cpm')); setkey(dt.fv1_NS.decent)
save(dt.fv1_NS.decent, file = 'data/fetal_venule_exp1_Nextseq.dt_decentCells.RData')


hist(rowSums(cast.fv1_NS.good > 0))

