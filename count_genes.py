# count hits to each gene. a gene count is incremented when all matched (SAM: "M") sections if and only if both mates of a paired-end read are
# aligned to that gene.

import sys
sys.path.append("/local10G/resources/py2.7.9_virtualenv/lib/python2.7/site-packages/")
import HTSeq as ht
import pandas as pd
from collections import Counter

input_bam=sys.argv[1]
output_prefix=sys.argv[2]
gtf_reference=sys.argv[3]

# perform cut -d';' -f 1 <ref.gtf> | awk '($3 ~ /exon/)' | uniq 
# before loading reference so that every (correctly ordered) exon appears
# only once


# If the gtf file has genes with >1 name or overlapping genes, reads
# that map to those may not be counted
# Sequentially name non-contiguous exons for each gene
# feature.name defaults to gene_id

i=0;
MIN_EXON_SEP=1

genes=ht.GenomicArrayOfSets("auto",stranded=False)
gtf_file=ht.GFF_Reader(gtf_reference)
for feature in gtf_file:
    if feature.type == "exon":
        genes[feature.iv]+=feature.name

# function to count gene hits
in_reads=read_pairs=ht.pair_SAM_alignments_with_buffer(ht.BAM_Reader(input_bam))
counts=Counter([])
for pair in in_reads:
    (r1,r2)=pair
    if isinstance(r1, ht._HTSeq.SAM_Alignment) and isinstance(r2,ht._HTSeq.SAM_Alignment):
        matched_genes=list([]); i=0
        iv_seq_1 = ( co.ref_iv for co in r1.cigar if co.type == "M" and co.size > 0 )
        iv_seq_2 = ( co.ref_iv for co in r2.cigar if co.type == "M" and co.size > 0 )
        for iv in iv_seq_1:
          for iv2, fs2 in genes[ iv ].steps():
            matched_genes.append(set([gene for gene in fs2]))
            i+=1
        for iv in iv_seq_2:
          for iv2, fs2 in genes[ iv ].steps():
            matched_genes.append(set([gene for gene in fs2]))
            i+=1

        # all matched subsequences of read must align to the same gene
        gene_names = set.intersection(*matched_genes) 
        if gene_names:
            counts[list(gene_names)[0]]+=1

# save output as a tab-delimited table
count_tuple=[tuple([gene,counts[gene]]) for gene in counts]
count_pd = pd.DataFrame(count_tuple, columns=['gene','count'])
count_pd.to_csv(output_prefix+".gene_counts.tsv", sep="\t", header=False,index=False)

