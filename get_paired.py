import sys
sys.path.append("/local10G/resources/py2.7.9_virtualenv/lib/python2.7/site-packages/")
input_bam=sys.argv[1]
output_prefix=sys.argv[2]
gtf_ref=sys.argv[3]

# Extract reads where both reads of pair are good reads and align to the same gene.

# load gene annotations
import HTSeq as ht
print "loading genome annotation"
gtf_file=ht.GFF_Reader(gtf_ref)

genes=ht.GenomicArrayOfSets("auto",stranded=False)
for feature in gtf_file:
    if feature.type == "exon":
        genes[feature.iv] += feature.name


good_reads=ht.BAM_Reader(input_bam)
read_pairs=ht.pair_SAM_alignments_with_buffer(good_reads)
good_paired_writer=ht.BAM_Writer.from_BAM_Reader(output_prefix+".paired.bam",good_reads)
for pair in read_pairs:
    (r1,r2)=pair
    
    # check that both in the pair are "good" reads
    if isinstance(r1, ht._HTSeq.SAM_Alignment) and isinstance(r2,ht._HTSeq.SAM_Alignment):
        # check that both align to the same gene
        read_genes1=set([]); read_genes2=set([])
        iv_seq1 = ( co.ref_iv for co in r1.cigar if co.type == "M" and co.size > 0 )
        iv_seq2 = ( co.ref_iv for co in r2.cigar if co.type == "M" and co.size > 0 )
        
        for iv in iv_seq1:
          for iv2, fs2 in genes[ iv ].steps():
            read_genes1 |= fs2
        for iv in iv_seq2:
          for iv2, fs2 in genes[ iv ].steps():
            read_genes2 |= fs2
            
        # if there are no differences in the aligned genes, write
        if not read_genes1.difference(read_genes2):
            good_paired_writer.write(r1)
            good_paired_writer.write(r2)

