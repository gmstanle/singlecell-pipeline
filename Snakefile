# Usage:
# snakemake --cluster "sbatch --job-name={params.name} --ntasks=1 --cpus-per-task={threads} \
#           --partition={params.partition} --mem={params.mem} -o slurm_output/{params.name}-%j \
#            -p -T -k -j 100 -w 100

import pandas as pd
import os

GTF_REF="/local10G/references/mm10-ERCC-Cre-Tdtom-Gfp-3/mm10-ERCC-Cre-Tdtom-Gfp-3.gtf"
GENOME_DIR="/local10G/references/mm10-ERCC-Cre-Tdtom-Gfp-3/mm10-ERCC-Cre-Tdtom-Gfp-3_STAR_FULL_ANNOTATED/"

df = pd.read_table("singlecell-pipeline/mSSC.samples_test.txt", index_col=0, comment="#")
df.index = df.index.astype(str)
VERSION = "1.0"

localrules: all

#df2 = pd.DataFrame({ "Project" : "", "R1":"","R2":"", "Sample" : df.index.unique()})
#df2 = df2.set_index(['Sample'])
#prev_index = ""
#for s in df.index:
#  if s != prev_index:
#    i=0
#    df2.ix[s,"Project"] = df.ix[s,"Project"].ix[i]
#    prev_index = s
#  
#  df2.ix[s,df.ix[s,"Read"].ix[i]]+= ","+df.ix[s,"Filename"].ix[i]
#  prev_index = s
#  i+=1
#
#for s in df2.index:
#  for rt in ['R1','R2']:
#    if df2.ix[s, rt].startswith(","):
#      df2.ix[s,rt] = df2.ix[s,rt][1:]

samples=[("".join(df.ix[s,"Project"].unique()),s) for s in set(df.index)]

rule all:
  input:  expand("pipeline/{sample[0]}/{sample[1]}/{sample[1]}.good_reads.paired.jun_counts.df",sample=samples),
          expand("pipeline/{sample[0]}/{sample[1]}/{sample[1]}.good_reads.paired.gene_counts.df",sample=samples),
          expand("pipeline/{sample[0]}/{sample[1]}/{sample[1]}.SJ.out.tab",sample=samples),
          expand("pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Log.out",sample=samples),
          expand("pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Log.progress.out",sample=samples),
          expand("pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Log.final.out",sample=samples)
  params: name='all', partition='general', mem='2000'

def input_files(wildcards):
  s = wildcards["sample"]
  r1 = list(df.loc[s,'Filename'][df.ix[s,'Read']=="R1"])
  r2 = list(df.loc[s,'Filename'][df.ix[s,'Read']=="R2"])

  return r1 + r2

rule concatenate_lanes:
  input: input_files
  output: temp("pipeline/{project}/{sample}/raw/{sample}.R1.fastq"),
    temp("pipeline/{project}/{sample}/raw/{sample}.R2.fastq")
  params: name="concatenate-{sample}", partition="general",mem="2048"
  threads: 2
  version: "0.11.2"
  
  run:
    s = wildcards.sample
    input[0] = " ".join(list(df.loc[s,'Filename'][df.ix[s,'Read']=="R1"]))
    input[1] = " ".join(list(df.loc[s,'Filename'][df.ix[s,'Read']=="R2"]))
    
    shell("zcat {input[0]} > {output[0]}")
    shell("zcat {input[1]} > {output[1]}")

rule star_align:
  input:  "{dir}/raw/{sample}.R1.fastq", "{dir}/raw/{sample}.R2.fastq"
  output: "{dir}/{sample}.Aligned.sortedByCoord.out.bam","{dir}/{sample}.SJ.out.tab",
    "{dir}/{sample}.Log.out","{dir}/{sample}.Log.progress.out","{dir}/{sample}.Log.final.out"
  params: name="star-{sample}", partition="general", mem="64000"
  threads: 12
  log:    "{dir}/{sample}.star.stderr.log"
  
  run:
    SCRATCH, orig_output, input, output = local_sync(input,output)
    
    # input should be uncompressed so we don't need --readFilesCommand zcat option
    # $ADAPTERS input might remove some false positive adapter-genome alignments
    # NOTE: load and remove only removes shared memory if no other processes are using the shared segment (per Alex Dobin)
    # To guarantee release of memory, "Remove" option should be used at the termination of job to free memory
    # ?Is there a way to load only if the genome is not already loaded?
    # Do sorting and markduplicates if you're running one node
    shell("echo {SCRATCH}/{wildcards.sample}.")
    shell("/usr/bin/time -v /local10G/gmstanle/tools/STAR-STAR_2.4.2a/bin/Linux_x86_64/STAR \
          --genomeDir {GENOME_DIR} \
          --genomeLoad LoadAndRemove \
          --readFilesIn {input[0]} {input[1]} \
          --outSAMstrandField intronMotif \
          --outSAMtype BAM SortedByCoordinate \
          --limitBAMsortRAM 20000000000 \
          --bamRemoveDuplicatesType UniqueIdentical \
          --outSAMunmapped Within \
          --outFileNamePrefix {SCRATCH}/{wildcards.sample}. \
          --runThreadN 12 \
          --outFilterMatchNminOverLread 0.25")
    pop(output,orig_output)


