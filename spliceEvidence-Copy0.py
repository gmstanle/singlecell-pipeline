
# coding: utf-8

# In[4]:

import HTSeq as ht
import itertools
import os

from matplotlib import pyplot
os.chdir("/Users/geoffstanley/Documents/QuakeLab/PacBio")


# In[6]:

# perform cut -d';' -f 1 <ref.gtf> | awk '($3 ~ /exon/)' | uniq 
# before loading reference so that every (correctly ordered) exon appears
# only once
gtf_file=ht.GFF_Reader("mm10-ERCC-Cre-Tdtom-Gfp-3_Exons.gtf")

# If the gtf file has genes with >1 name or overlapping genes, reads
# that map to those may not be counted
# Sequentially name non-contiguous exons for each gene
# feature.name defaults to gene_id

i=0;
MIN_EXON_SEP=1

exon_num={}
prev_end={}
exons=ht.GenomicArrayOfSets("auto",stranded=False)
for feature in gtf_file:
    if feature.type == "exon":
        exon_num[feature.name]=0
        prev_end[feature.name]=0
    
for feature in gtf_file:
     # initialize prev_name and end
    if feature.type == "exon":

        if (feature.iv.start - prev_end[feature.name]) > MIN_EXON_SEP: 
            exon_num[ feature.name ]+=1
        
        exon_name = feature.name + "_e" + format(exon_num[feature.name],'02')
        prev_end[feature.name]=feature.iv.end
        exons[ feature.iv ] += exon_name


# In[5]:

# function to count reads and junctions that skip an exon
def count_with_skips(in_reads, exon_reference, excl_nonCanonical, safety_margin):
    # initialize count file
    counts = {}
    skip_counts = {}
    for iv, fs in exon_reference.steps():
        if len(fs)==1:
            counts[fs] = 0
            skip_counts[fs] = 0
    
    for read in in_reads
        for a in read.cigar: # add counts to exons that are skipped

            # prevent non-canonical junctions from being counted
            # get a list of exons that align to the matched part of the read
            # these exons will be excluded from skip_counts for this read
            # non-canonical junctions may arise from RT/polymerase "jumping" due to e.g. 2ndary struct.
            # or an inaccurate reference
            matched_exons=set([])

            iv_seq = ( co.ref_iv for co in read.cigar if co.type == "M" and co.size > 0 )
            for iv in iv_seq:
              for iv2, fs2 in exons[ iv ].steps():
                matched_exons |= fs2
            for exon2 in matched_exons:
                counts[exon2]+=1

        for a in read.cigar:
            if a.type == "N":
                for iv, val in exons[ht.GenomicInterval(a.ref_iv.chrom,a.ref_iv.start+safety_margin,                                                        a.ref_iv.end-safety_margin)].steps():
                    if len(val) > 0 and (val not in matched_exons or (not excl_nonCanonical)):
                        skip_counts[ list(val)[0] ]+=1
    return(counts, skip_counts)


# I think the best strategy would be to track the type of junction read based on the conditions below, rather than a binary good/bad read. And possibly which exons the read overlaps. Then counting would be a separate function and I could just call it on the different output files (i.e. reads with different conditions). Which read types would be useful? 
#  - NOT chimeras, intron-only reads, multi-gene reads
#  - YES canonical exon reads, non-canonical exon reads
#      - non-canonical are ~20% as abundant as canonical - would be interesting if we decide we care about splicing
#      but want to move to single-ended reads
#  - MAYBE: reads that are canonical when single-ended but mate is intronic 

# In[30]:

all_reads=ht.BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.Aligned.sortedByCoord.out.bam")
good_reads_writer=     ht.BAM_Writer.from_BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.good_reads.bam",all_reads)
good_jun_reads_writer=     ht.BAM_Writer.from_BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.good_jun_reads.bam",all_reads)
intronic_reads_writer=    ht.BAM_Writer.from_BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.intronic_reads.bam",all_reads)
mult_reads_writer=    ht.BAM_Writer.from_BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.mult_aligned_reads.bam",all_reads)
skipped_iv=ht.GenomicArray("auto",stranded=False,typecode="i")
c11=0; c12=0; c21=0; c10=0; c00=0; num_good_reads=0; num_bad_reads=0; nh2=0; num_jun_reads=0;

bad_cigars=set(['I','S','H'])
for read in all_reads:
   
    
    chunk=ht.GenomicArrayOfSets("auto",stranded=False)
    notstarted=True
    is_good_read=True
    is_jun_read=False
    has_intronic=False
    has_mult_algn=False
    # check that there is only 1 unique mapping
    # Warning: this depends on the 'NH' field existing (works w/Tophat2)
    opts=dict(read.optional_fields)
    if opts["NH"] > 1:
        is_good_read=False
        nh2+=1
    else:
        num_sections=0
        for a in read.cigar: # does this read have a skipped part?
            prev_gene=''
            num_sections+=1
            if a.type == "N":
                is_jun_read=True
                num_jun_reads+=1

        if is_jun_read:
            section_num=0
            chunkend=0; chunkstart=0
            for a in read.cigar:
                section_num+=1
                if a.type != "N" and a.type not in bad_cigars and section_num != num_sections:
                    # get the position of the start of the chunk
#                     print section_num, a.type
                    if notstarted:
                        chunk_fs = set([])
                        notstarted=False
                    for iv, fs in exons[ a.ref_iv ].steps():
                        # if a chunk extends past exon,
                        # the read is bad
                        if len(fs)==0:
                            is_good_read=False
                        else:
                            chunk_fs |= fs
                    
    #                         chunkstart=a.ref_iv.start
                    
                # if we are on the skipped part or end of alignment
                else: 
                    if a.type=="N":
#                         chunkend=a.ref_iv.start # previous chunk ends at beginning of the skip
                        notstarted=True
                    elif a.type not in bad_cigars:
                        if notstarted:
                            chunk_fs = set([])
                            notstarted=False
                        for iv, fs in exons[ a.ref_iv ].steps():
                            # if a chunk extends past exon,
                            # the read is bad
                            if len(fs)==0:
                                is_good_read=False
                            else:
                                chunk_fs |= fs

                    # check whether the skip occurs at the start/end of an exon and 
#                     # thus whether the junction is canonical
#                     for iv, val in exons[chunk_iv].steps():
#                         chunk_len+=1
#                         chunk_set |= val
#                     if len(chunk_fs)==0:
#                         is_good_read=False
#                         c10+=1
                        # this is the vast majority of bad chunks - 
                        # align wholly to non-exonic part of genome
                    if is_good_read and len(chunk_fs)==1:
                        c11+=1
#                         print prev_gene, list(chunk_fs)[0][:-4]
                        # check that previous chunk was on same gene
                        if (list(chunk_fs)[0][:-4] != prev_gene) and (prev_gene != ""):
                            is_good_read=False
#                             raise StandardError
#                             print "2-gene splice"

                        else:
                            prev_gene = list(chunk_fs)[0][:-4]
#                             print "prev_gene is ", prev_gene, section_num, " out of ", num_sections, a.type

                            # if we shrink the skipped range by 1 codon on either
                            # side, it should overlap nothing. If it is 
                            # expanded by 1 codon on either side, it should overlap
                            # 2 exons
                            # may want to record this and track separately
#                             if a.type=="N":
#                                 shrunk=ht.GenomicInterval(read.iv.chrom,a.ref_iv.start+4,a.ref_iv.end-4)
#                                 shrunk_set=set();
#                                 for iv, val in exons[shrunk].steps():
#                                     shrunk_set |= val

#                                 expanded=ht.GenomicInterval(read.iv.chrom,a.ref_iv.start-1,a.ref_iv.end+1)
#                                 expanded_set=set();
#                                 for iv, val in exons[expanded].steps():
#                                     expanded_set |= val

#                                 if len(expanded_set)<len(shrunk_set)+2:
#                                     is_good_read=False
# #                                     raise StandardError
#                                 elif len(expanded_set)>len(shrunk_set)+2:
#                                     print "weird skipped test - check for redundant exon annotations"
#                                     raise StandardError
                    elif is_good_read and len(chunk_fs) > 1:
                        is_good_read=False
#                         raise StandardError
                        # most of these are on parts of genome w/multiple 
                        # gene_id annotations
                        c12+=1
#                     elif chunk_len > 1 and len(chunk_set)==1:
#                         is_good_read=False
#                         raise StandardError
#                         # most of these chunks seem ok - junctions are in correct places but the non-junction
#                         # end extends a bit past the annotated exon - looks more like poor annotation
#                         c21+=1
                    elif is_good_read:
                        is_good_read=False
                        c00+=1
            if is_good_read:
#                 raise StandardError
                # reads are being written as rev. comp. if they are aligned to - strnd
                good_jun_reads_writer.write(read)
#                 for a in read.cigar: # add counts to exons that are skipped
#                     if a.type == "N":
#                         for iv, val in exons[ht.GenomicInterval(a.ref_iv.chrom,a.ref_iv.start+3,a.ref_iv.end-3)].steps():
#                             if len(val) == 1: #>1 would be "ambiguous"
#                                 skip_counts[ list(val)[0] ]+=1

        else:# if there is no skipped part in read
            read_exons=set()
            read_genes=str()
            iv_seq = ( co.ref_iv for co in read.cigar if co.type == "M" and co.size > 0 )
            for iv in iv_seq:
              for iv2, fs2 in exons[ iv ].steps():
                # check if read has any alignment to intronic (genomic) seq
                if not len(fs2): 
                    is_good_read=False
                    has_intronic=True
                else:
                    read_exons |= fs2
                    read_genes += ","+list(fs2)[0][:-4]
            
            # test if read aligns to multiple genes
            read_genes=set(read_genes.split(","))
            if len(read_genes) > 2 and is_good_read:
                is_good_read=False
                has_mult_align=True
            
        
    if is_good_read: # write good reads and add count to exons
        good_reads_writer.write(read)
        num_good_reads+=1
    elif has_intronic:
        intronic_reads_writer.write(read)
    elif has_mult_algn:
        mult_reads_writer.write(read)
        
        
#         iv_seq = ( co.ref_iv for co in read.cigar if co.type == "M" and co.size > 0 )
#         fs = set([])
#         for iv in iv_seq:
#           for iv2, fs2 in exons[ iv ].steps():
#             fs |= fs2
#         for exon2 in fs:
#             counts[exon2]+=1
    else:
        num_bad_reads+=1


## Extract reads where both reads of pair are good reads and align to the same gene.

# In[7]:

# load gene annotations
gtf_file=ht.GFF_Reader("mm10-ERCC-Cre-Tdtom-Gfp-3_Exons.gtf")

genes=ht.GenomicArrayOfSets("auto",stranded=False)
for feature in gtf_file:
    if feature.type == "exon":
        genes[feature.iv] += feature.name


# In[9]:

good_reads=ht.BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.good_reads.bam")
read_pairs=ht.pair_SAM_alignments_with_buffer(good_reads)
good_paired_writer=ht.BAM_Writer.from_BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.paired_good_reads.bam",good_reads)
unpaired_writer=ht.BAM_Writer.from_BAM_Reader("1-0-0-0-BTN24-C29-18ul-IL5195-708-502.unpaired_good_reads.bam",good_reads)
for pair in read_pairs:
    (r1,r2)=pair
    
    # check that both in the pair are "good" reads
    if isinstance(r1, ht._HTSeq.SAM_Alignment) and isinstance(r2,ht._HTSeq.SAM_Alignment):
        # check that both align to the same gene
        read_genes1=set([]); read_genes2=set([])
        iv_seq1 = ( co.ref_iv for co in r1.cigar if co.type == "M" and co.size > 0 )
        iv_seq2 = ( co.ref_iv for co in r2.cigar if co.type == "M" and co.size > 0 )
        
        for iv in iv_seq1:
          for iv2, fs2 in genes[ iv ].steps():
            read_genes1 |= fs2
        for iv in iv_seq2:
          for iv2, fs2 in genes[ iv ].steps():
            read_genes2 |= fs2
            
        # count all overlapping
        if not read_genes1.difference(read_genes2):
            good_paired_writer.write(r1)
            good_paired_writer.write(r2)
        
    # if mate isn't a good read (not found in good_read file)
    # then write to unpaired file
    elif isinstance(r1,ht._HTSeq.SAM_Alignment):
        unpaired_writer.write(r1)
    elif isinstance(r2,ht._HTSeq.SAM_Alignment):
        unpaired_writer.write(r2)


# In[ ]:




# In[53]:

for read in itertools.islice(all_reads,1):
    print read
    print r1


# In[123]:

# perform counting on good paired (gp) reads only
gp_reader=ht.BAM_Reader("1-0-0-0-BTN22-C34-8ul-1.paired_good_reads.bam")

for read in gp_reader:
    iv_seq = ( co.ref_iv for co in read.cigar if co.type == "M" and co.size > 0 )
    fs = set([])
    for iv in iv_seq:
      for iv2, fs2 in exons[ iv ].steps():
        fs |= fs2
    for exon2 in fs:
        counts_gp[exon2]+=1
            
    for a in read.cigar: # add counts to exons that are skipped
        if a.type == "N":
            for iv, val in exons[ht.GenomicInterval(a.ref_iv.chrom,a.ref_iv.start+3,a.ref_iv.end-3)].steps():
                if len(val) == 1: 
                    skip_counts_gp[ list(val)[0] ]+=1
    


# In[124]:

print chunk_fs,is_good_read, a.type, num_sections, section_num, a.ref_iv,prev_gene, read


# In[125]:

for name in counts:
    if skip_counts[name]>1 and counts[name]>1:
            print name, counts[name], skip_counts[name]


# In[126]:

for name in counts_gp:
    if skip_counts_gp[name]>1 and counts_gp[name]>1:
            print name, counts_gp[name], skip_counts_gp[name]


# In[133]:

# get statistics on counts
# number of genes detected with and without using read pair informationb
num_detected={}; num_detected["exons"]=0; num_detected["exons_gp"]=0; num_detected["skipped"]=0; num_detected["skipped_gp"]=0
for name in counts:
    if counts[name]>0: num_detected["exons"]+=1 
    if counts_gp[name]>0:  num_detected["exons_gp"]+=1
    if skip_counts[name]>0: num_detected["skipped"]+=1
    if skip_counts_gp[name]>0: num_detected["skipped_gp"]+=1 
        
print num_detected


# In[1]:

# get length distributions of exons to see where read/insert length should be to minimize false positive rate
# get length distributions of exons that were filtered with pair information and compare to 
# exons detected w/o pair info

import matplotlib.pyplot as plt
import pandas as pd
df = pd.DataFrame({'all_counts':pd.Series(counts), 'all_skip_counts':pd.Series(skip_counts), 'gp_counts':pd.Series(counts_gp),
                'gp_skip_counts':pd.Series(skip_counts_gp)})


# In[30]:

r1=['a','b','b']
pd.Series(r1)
rt=['r1','r1','r2']


# In[195]:

sample_dir="/datastore/gmstanle/sequencing-data/raw-data/DataSingleCellRNAseq_Brain/nextseq_raw/"
df = pd.read_table("/Users/geoffstanley/quakestor/scripts/singlecell_samples.txt", index_col=0, comment="#")
df2 = pd.DataFrame({ "Project" : "", "R1":"","R2":"", "Sample" : df.index.unique()})
df2 = df2.set_index(['Sample'])
prev_index = ""
for s in df.index:
    if s != prev_index:
        i=0
        df2.ix[s,"Project"] = df.ix[s,"Project"][i]
        prev_index = s

    df2.ix[s,df.ix[s,"Read"][i]]+= ","+ sample_dir + df.ix[s,"Filename"][i]
    prev_index = s
    i+=1

for s in df2.index:
    for rt in ['R1','R2']:
        if df2.ix[s, rt].startswith(","):
            df2.ix[s,rt] = df2.ix[s,rt][1:]


# In[203]:

df3 = df2[:5]
[" ".join(df2.ix[s,"R1"].split(",")), " ".join(df2.ix[s,"R2"].split(","))]


# In[ ]:



