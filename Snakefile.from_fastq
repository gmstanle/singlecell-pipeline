# Notes for running on Sherlock vs. singlecell
# In Snakefile.utils, change "LOCAL_SCRATCH" to "LOCAL_SATA"
# Change USER_HOME 

# Usage:
# snakemake --cluster "sbatch --job-name={params.name} --ntasks=1 --cpus-per-task={threads} \
#           --partition={params.partition} --mem={params.mem} -o slurm_output/{params.name}-%j \
#            -p -T -k -j 100 -w 100

# Changelog:
# 3/27/17: Making this compatible with Sherlock (sparse genomes, 32G + 8 cores)
# Also making it compatible with reads that have already been concatenated
# 4/20/17: Cannot find bug in samtools name sort + htseq-count - rely on STAR gene counts for now
# which are equivalent to htseq-count union (probably too permissive, but hey at least it's not Kallisto!)
#
#
import pandas as pd
import os

#GTF_REF="/local10G/references/mm10-ERCC-Cre-Tdtom-Gfp-3/mm10-ERCC-Cre-Tdtom-Gfp-3.gtf"
#GENOME_DIR="/local10G/references/mm10-ERCC-Cre-Tdtom-Gfp-3/mm10-ERCC-Cre-Tdtom-Gfp-3_STAR_FULL_ANNOTATED/"

# USER_HOME="/scratch/users/gmstanle"
USER_HOME="/local10G/gmstanle"
ANACONDA=USER_HOME+"/programs/anaconda2"

GTF_REF=   USER_HOME+"/references/Flag_Myc_Nr2f2_OE/mm10-ERCC-Cre-Tdtom-Gfp-MycFlag.gtf"
GENOME_DIR=USER_HOME+"/references/Flag_Myc_Nr2f2_OE/STAR_mm10-ERCC-Cre-Tdtom-Gfp-MycFlag"

PIPE_DIR=USER_HOME+"/singlecell"
# input data must have 4 colums: Sample, Read, Filename, Project
# Sample = sample name - this will be the prefix of all output files
# Read = "R1" or "R2"
# Filename = /path/to/{sample}.file.fastq.gz
# Project = folder that all output files will go in
df = pd.read_table(USER_HOME+"/singlecell/singlecell-pipeline/mHSC_H5P2_Hoxb5_NovaSeq.samples.txt", index_col=0, comment="#")
df.index = df.index.astype(str)
VERSION = "1.0"

include: "Snakefile.utils"
include: "Snakefile.qc"

localrules: all

samples=[("".join(df.ix[s,"Project"].unique()),s) for s in set(df.index)]

rule all:
  input:  
          expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Aligned.out.bam",sample=samples,base=PIPE_DIR),
          expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.SJ.out.tab",sample=samples,base=PIPE_DIR),
          expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Log.out",sample=samples,base=PIPE_DIR),
          expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Log.progress.out",sample=samples,base=PIPE_DIR),
          expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.Log.final.out",sample=samples,base=PIPE_DIR),
          expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.ReadsPerGene.out.tab",sample=samples,base=PIPE_DIR)
      #    expand("{base}/pipeline/{sample[0]}/{sample[1]}/{sample[1]}.htseq_count.df",sample=samples,base=PIPE_DIR)
  params: name='all', partition='general', mem='2000'

def input_files(wildcards):
  s = wildcards["sample"]
  r1 = list(df.loc[s,'Filename'][df.ix[s,'Read']=="R1"])
  r2 = list(df.loc[s,'Filename'][df.ix[s,'Read']=="R2"])

  return r1 + r2

rule concatenate_lanes:
  input: input_files
  output: temp("{base}/pipeline/{project}/{sample}/raw/{sample}.R1.fastq"),
    temp("{base}/pipeline/{project}/{sample}/raw/{sample}.R2.fastq")
  params: name="concatenate-{sample}", partition="general",mem="2048"
  threads: 1
  version: "0.11.2"
  
  run:
    s = wildcards.sample
    input[0] = " ".join(list(df.loc[s,'Filename'][df.ix[s,'Read']=="R1"]))
    input[1] = " ".join(list(df.loc[s,'Filename'][df.ix[s,'Read']=="R2"]))
    
    shell("zcat {input[0]} > {output[0]}")
    shell("zcat {input[1]} > {output[1]}")

# 3/27/17: Change star_align to assume lanes have already be concatenated.
# If lanes are separated, this needs to change!
# Changed to 24G memory, 6 nodes, for Sherlock + sparse genome index
# Note 4/1/17: STAR appears to have max. RSS of 17G for sparse genome. 
# Reduce requested mem to 20G, 5 cores.
rule star_align:
  #input:  "{dir}/raw/{sample}.R1.fastq", "{dir}/raw/{sample}.R2.fastq" 
  input: input_files
  #output: "{base}/pipeline/{project}/{sample}/{sample}.Aligned.out.bam",
  #        "{base}/pipeline/{project}/{sample}/{sample}.SJ.out.tab",
  #        "{base}/pipeline/{project}/{sample}/{sample}.Log.out",
  #        "{base}/pipeline/{project}/{sample}/{sample}.Log.progress.out",
  #        "{base}/pipeline/{project}/{sample}/{sample}.Log.final.out"
  output: "{dir}/{sample}.Aligned.out.bam",
           "{dir}/{sample}.SJ.out.tab",
           "{dir}/{sample}.Log.out",
           "{dir}/{sample}.Log.progress.out",
           "{dir}/{sample}.Log.final.out",
           "{dir}/{sample}.Chimeric.out.sam",
           "{dir}/{sample}.Chimeric.out.junction",
           "{dir}/{sample}.ReadsPerGene.out.tab"
  params: name="star-{sample}", partition="general", mem="32000"
  threads: 6
  #log:    "{USER_HOME}/pipeline/{project}/{sample}/{sample}.star.stderr.log"
  log:    "{dir}/{sample}.star.stderr.log"
  
  run:
    # think this is a waste of time - reads and reference loaded into memory only once. why load into SCRATCH first?
    # Actually, may need to work in the scratch space - looks like temporary files may be going in my home directory with no
    # unique names, causing errors when multiple jobs running.
    s = wildcards.sample
    input[0] = " ".join(list(df.loc[s,'Filename'][df.ix[s,'Read']=="R1"]))
    input[1] = " ".join(list(df.loc[s,'Filename'][df.ix[s,'Read']=="R2"]))
    SCRATCH, orig_output, input, output = local_sync(input,output)
    #
    # SCRATCH = get_scratch()
    # if not concatenating w/Snakemake,.fastq will be compressed .gz files - need to use STAR --readFilesCommand
    
    # input should be uncompressed so we don't need --readFilesCommand zcat option
    # $ADAPTERS input might remove some false positive adapter-genome alignments
    # NOTE: load and remove only removes shared memory if no other processes are using the shared segment (per Alex Dobin)
    # To guarantee release of memory, "Remove" option should be used at the termination of job to free memory
    # ?Is there a way to load only if the genome is not already loaded?
    # Do sorting and markduplicates if you're running one node
    #
    # 03/27/17: Updating for Sherlock cluster. Will be using sparse index, 32G memory, 8 cores.
    # 03/27/17: Updating to add generation of Chimera file for aligning to fusion genes (e.g. Flag-Myc-Nr2f2 gene)
    shell("source {ANACONDA}/bin/activate root && "
          "/usr/bin/time -v STAR "
          "--genomeDir {GENOME_DIR} "
          "--genomeLoad NoSharedMemory "
          "--readFilesIn {input[0]} {input[1]} "
          "--readFilesCommand zcat "
          "--outSAMstrandField intronMotif "
          "--outSAMtype BAM Unsorted "
#          "--limitBAMsortRAM 18000000000 "
          "--bamRemoveDuplicatesType UniqueIdentical "
          "--outSAMunmapped Within "
          "--outFileNamePrefix {SCRATCH}/{wildcards.sample}. "
          "--runThreadN 6 "
          "--quantMode GeneCounts "
          "--chimSegmentMin 20 2>{log}")
    pop(output,orig_output)


rule htseq_count:
  input: "{dir}/{sample}.Aligned.out.bam"
  output: "{dir}/{sample}.htseq_count.df"
  params: name="ht-count.{sample}", partition="general", mem="32000"
  threads: 6
  log:    "{dir}/{sample}.ht_count.stderr.log"

  run:
    SCRATCH, orig_output, input, output = local_sync(input,output)
    
      # sort .bam by name and output as .sam. Giving it 4G of mem just because Sherlock will not give < 4G for 1 core.
      # count reads with htseq-count, intersection-strict and non-stranded
    shell("source {ANACONDA}/bin/activate {ANACONDA} && "
          "samtools sort -@ 6 -n -m 30G -T {SCRATCH}/{wildcards.sample} {input} -o {SCRATCH}/{wildcards.sample}.Aligned.sortedByName.bam 2>{log}")
          
    shell("htseq-count -f bam -s no -m intersection-strict {SCRATCH}/{wildcards.sample}.Aligned.sortedByName.bam {GTF_REF} > "
          "{SCRATCH}/{wildcards.sample}.htseq.txt 2>>log}")
    
#    shell("source {ANACONDA}/bin/activate {ANACONDA} && "
#          "samtools sort -@ 6 -n -m 30G -T {SCRATCH}/tmp/{wildcards.sample} {input} | samtools view - | "
#          "htseq-count -s no -m intersection-strict - {GTF_REF} > "
#          "{SCRATCH}/{wildcards.sample}.htseq.txt 2>{log}")
      # add sample names to count file
    shell("sh {USER_HOME}/singlecell/make_df.sh {SCRATCH}/{wildcards.sample}.htseq.txt {wildcards.sample} > {output} 2>>{log}")
    pop(output,orig_output)
