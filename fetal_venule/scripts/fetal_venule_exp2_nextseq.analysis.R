rm(list=ls())
require(data.table)
require(ggplot2)
require(reshape2)
require(RColorBrewer)
require(plyr)
require(doMC)
registerDoMC(6)
source("~/singlecell-pipeline/rnaseq_funcs_V2.R")
setwd('~/singlecell-pipeline/fetal_venule/exp2_nextseq/')

load('data/fetal_venule_exp2.dt_goodCells.RData')
load('data/fetal_venule_exp2.cell_info_dt.RData')


# munge -----
# remove genes not detected in dataset
load('data/fetal_venule_exp2_Nextseq.htseq_counts.tab')
cast.exp2 = as.matrix(cast.all); rm(cast.all) # really, should use a matrix object here

numcells = colSums(cast.exp2 > 0)
genes.Detected = names(numcells[numcells > 0])
cast.exp2.noTechnical = cast.exp2[,genes.Detected]

htseq = colnames(cast.exp2.noTechnical)[colnames(cast.exp2.noTechnical) %like% "X__"]
htseq.unaligned = cast.exp2.noTechnical[,htseq]
save(htseq.unaligned, file = 'data/fetal_venule_exp2.htseq_unaligned.RData')

ercc = colnames(cast.exp2.noTechnical)[colnames(cast.exp2.noTechnical) %like% "ERCC.0"]
ercc.reads = cast.exp2.noTechnical[,ercc]
save(ercc.reads, file = 'data/fetal_venule_exp2.ercc_reads.RData')

ncol(cast.exp2.noTechnical)
cast.exp2.noTechnical = cast.exp2.noTechnical[, !colnames(cast.exp2.noTechnical) %in% c(htseq, ercc)]
ncol(cast.exp2.noTechnical)

# normalize to counts per million
numcounts = rowSums(cast.exp2.noTechnical)
cast.exp2.cpm = 1e6 * cast.exp2.noTechnical / numcounts
summary(rowSums(cast.exp2.cpm)) # every cell should have rowSum = 1e6

cast.exp2.log10.cpm = log10(cast.exp2.cpm + 1)

hist(cast.exp2.log10.cpm[, 'Mki67'])

rm(cast.exp2.cpm)
# add Flag_myc Chimera counts ----
flagmyc.raw = fread('data/fetal_venule_exp2_Nextseq.Flag_myc_chimeraCounts.txt')
setkey(flagmyc.raw); setnames(flagmyc.raw, c('Flag_Nr2f2_fusion','cell.name'))
flagmyc.raw = data.frame(flagmyc.raw); rownames(flagmyc.raw) = flagmyc.raw$cell.name
flagmyc.raw = flagmyc.raw[,'Flag_Nr2f2_fusion', drop = F]

flagmyc.raw = merge(data.frame(flagmyc.raw), data.frame(numcounts), by=0)
flagmyc.raw = transform(flagmyc.raw, row.names=Row.names, Row.names=NULL)
flagmyc.raw$Flag_Nr2f2_fusion.cpm       = 1e6 * flagmyc.raw$Flag_Nr2f2_fusion / flagmyc.raw$numcounts
flagmyc.raw$Flag_Nr2f2_fusion.log10.cpm = log10(flagmyc.raw$Flag_Nr2f2_fusion.cpm + 1)
hist(flagmyc.raw$Flag_Nr2f2_fusion.log10.cpm)
flagmyc.raw$Flag_Nr2f2_fusion = flagmyc.raw$Flag_Nr2f2_fusion.log10.cpm

cast.exp2.log10.cpm = merge(cast.exp2.log10.cpm, flagmyc.raw[,'Flag_Nr2f2_fusion', drop=F], by = 0, all = T)
cast.exp2.log10.cpm = transform(cast.exp2.log10.cpm, row.names=Row.names, Row.names=NULL)
cast.exp2.log10.cpm$Flag_Nr2f2_fusion[is.na(cast.exp2.log10.cpm$Flag_Nr2f2_fusion)] <- 0
hist(cast.exp2.log10.cpm$Flag_Nr2f2_fusion)

# pretty clear that some of thesea
pdf('qc/FlagChimera_FlagHTseq-count.pdf', height = 4, width = 4.5)
with(cast.exp2.log10.cpm, plot(Flag_Nr2f2_fusion, Flag_myc_transgene,
                               main = 'Log10 cpm of STAR Chimera vs HTseq-count'))

dev.off()


# what fraction of cells has detection in either the chimera or the 150bp standalone transgene?
# either: 326
# both:   163
# chimera: 277
# standalone: 212
with(cast.exp2.log10.cpm, sum(Flag_Nr2f2_fusion + Flag_myc_transgene > 0))
with(cast.exp2.log10.cpm, sum(Flag_Nr2f2_fusion*Flag_myc_transgene > 0))
with(cast.exp2.log10.cpm, sum(Flag_myc_transgene > 0))

rm(flagmyc.cpm, flagmyc.log10.cpm, flagmyc.raw)


# add metadata ----
# requires cast.exp2.noTechnical from munge step
exp2.info = fread('data/fetal_venule_exp2.plateInfo.csv')
exp2.cell2plate = fread('data/exp2_e13_5_Nr2f2_OE.cellname2NexteraPlate.txt')

exp2.cell2plate[,plate.barcode := substr(cell.name, 1, 11)]
setkey(exp2.cell2plate)

colnames(exp2.info) = c('plate.name','plate.barcode','sort.date','macro.plate','rt.date','pcr.date','aati.file',colnames(exp2.info)[8:ncol(exp2.info)])
setkey(exp2.info)

exp2.cell.info = merge(exp2.cell2plate, exp2.info[,.(plate.name,plate.barcode,sort.date,macro.plate,rt.date,pcr.date)], by='plate.barcode')

rm(exp2.cell2plate, exp2.info)
exp2.cell.info[,genotype:='WT']; exp2.cell.info[plate.name %like% 'COUPTFIIOE', genotype:='CoupTFII_OE']

# add statistics - make sure htseq 'genes' (starting with __) are not in matrix!!
sum.counts = rowSums(cast.exp2.noTechnical)
num.genes = rowSums(cast.exp2.noTechnical > 0)

exp2.info.df = data.frame(exp2.cell.info, row.names = 'cell.name')
exp2.info.df = merge(exp2.info.df, data.frame(sum.counts), by = 0)
exp2.info.df = transform(exp2.info.df, row.names=Row.names, Row.names=NULL)
exp2.info.df = merge(exp2.info.df, data.frame(num.genes), by = 0)
exp2.info.df = transform(exp2.info.df, row.names=Row.names, Row.names=NULL)

# transform into data.table format
exp2.cell.info.dt = data.table(exp2.info.df, cell.name = rownames(exp2.info.df))
setkey(exp2.cell.info.dt)

# add color labels
exp2.cell.info.dt[, genotype_color:=c('firebrick','cornflowerblue')[as.numeric(factor(genotype))]]
exp2.cell.info.dt[, nextera_color:=brewer.pal(length(unique(exp2.cell.info.dt[,nextera_plate])),'Set2')
                  [as.numeric(factor(nextera_plate))]]
exp2.cell.info.dt[, plate_color:=gg_color_hue(length(unique(exp2.cell.info.dt[,plate.barcode])))
                  [as.numeric(factor(plate.barcode))]]

# add htseq and ERCC technical metrics
load('data/fetal_venule_exp2.htseq_unaligned.RData')
htseq.technical = data.table(htseq.unaligned, cell.name=rownames(htseq.unaligned), htseq.sum = rowSums(htseq.unaligned))
exp2.cell.info.dt = merge(exp2.cell.info.dt, htseq.technical, by = 'cell.name')

load('data/fetal_venule_exp2.ercc_reads.RData')
ercc.technical = data.table(cell.name=rownames(ercc.reads), ercc.sum = rowSums(ercc.reads))
exp2.cell.info.dt = merge(exp2.cell.info.dt, ercc.technical, by = 'cell.name')

# save
setkey(exp2.cell.info.dt)
save(exp2.cell.info.dt, file='data/fetal_venule_exp2.cell_info_dt.RData')
rm(exp2.cell.info);

# qc, remove low qual cells -----
load('data/fetal_venule_exp2.cell_info_dt.RData')

ggplot(exp2.cell.info.dt, aes(log10(sum.counts + 1), num.genes, color = plate.name, shape = nextera_plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw() + geom_vline(xintercept = 5) + geom_hline(yintercept = 300)
ggsave('qc/num.genes_numReads_all.pdf', height = 5, width = 7)

exp2.cell.info.dt[,frac.uncounted := htseq.sum / (sum.counts + htseq.sum + ercc.sum)]

ggplot(exp2.cell.info.dt[sum.counts > 1e5], aes(num.genes, frac.uncounted, color = plate.name, shape = nextera_plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw()
ggsave('qc/numGenes_fractionUncounted_all.pdf', height = 5, width = 7)

exp2.cell.info.dt = merge(exp2.cell.info.dt, 
                          data.table(Rn45s = cast.exp2[,'Rn45s'], cell.name = rownames(cast.exp2)),
                          by = 'cell.name')

exp2.cell.info.dt[,frac.mRNA := 1 - (Rn45s + htseq.sum + ercc.sum)/(htseq.sum + ercc.sum + sum.counts)]
ggplot(exp2.cell.info.dt[sum.counts > 1e5], aes(num.genes, frac.mRNA, color = plate.name, shape = nextera_plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw()

exp2.cell.info.dt[,frac.rRNA := Rn45s/(sum.counts)]
ggplot(exp2.cell.info.dt[sum.counts > 1e5], aes(num.genes, frac.rRNA, color = plate.name, shape = nextera_plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw() + ggtitle('Fraction mapping to Rn45s\nall aligned non-ERCC reads')
ggsave('qc/numGenes_fraction_rRNA_all.pdf', height = 5, width = 7)

# only use cells with at least 10^5 reads and at least 300 genes detected
cells.use = exp2.cell.info.dt[sum.counts > 1e5][num.genes > 300, cell.name]

ggplot(exp2.cell.info.dt[cell.name %in% cells.use, ], aes(log10(sum.counts + 1), num.genes, color = plate.name, shape = nextera_plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw()
ggsave('qc/num.genes_numReads_all.pdf', height = 5, width = 7)

ggplot(exp2.cell.info.dt[cell.name %in% cells.use, ], aes(num.genes, frac.rRNA, color = plate.name, shape = nextera_plate)) + 
  geom_point(size = 2, alpha = 0.7) + theme_bw() + ggtitle('Fraction mapping to Rn45s\nall aligned non-ERCC reads')
ggsave('qc/numGenes_fractionUncounted_all.pdf', height = 5, width = 7)

cast.exp2.good = cast.exp2.log10.cpm[rownames(cast.exp2.log10.cpm) %in% cells.use, ]
with(cast.exp2.good, plot(Flag_myc_transgene, Flag_Nr2f2_fusion))

save(cast.exp2.good, file='data/fetal_venule_exp2.cast_goodCells_log10cpm.RData')

rm(cast.exp2.log10.cpm)
dt.exp2.good = data.table(melt(as.matrix(cast.exp2.good)))
setnames(dt.exp2.good, c('cell.name','gene','log10.cpm')); setkey(dt.exp2.good)

save(dt.exp2.good, file = 'data/fetal_venule_exp2.dt_goodCells.RData')



# Classify into WT, recomb, non.recomb ----
cells.cre = exp2.cell.info.dt[genotype!='WT',cell.name]
cast.exp2.cre = dcast.data.table(dt.exp2.good[cell.name %in% cells.cre], cell.name ~ gene, fill = 0)
mat.exp2.cre = as.matrix(cast.to.df(dt.exp2.good[cell.name %in% cells.cre]))
mat.exp2.WT = as.matrix(cast.to.df(dt.exp2.good[!cell.name %in% cells.cre]))

# how many cells fit into the various ways of calling recombined cells?

# by Flag-myc expression (either fusion or standard alignment): 326/729 (44% of cells)
sum(mat.exp2.cre[,'Flag_Nr2f2_fusion'] + mat.exp2.cre[,'Flag_myc_transgene'] > 0)

# by setting a cutoff of Nr2f2 expression: 469 (65% of cells)
# Problem with this method is that many cells in WT animals will fall above this threshold
# pdf('transgene_info/recombinedCutoff_FlagMyc_Nr2f2.pdf', height = 4, width = 4.5)
#   plot(mat.exp2.cre[,'Nr2f2'], mat.exp2.cre[,'Flag_Nr2f2_fusion']+mat.exp2.cre[,'Flag_myc_transgene'],
#        xlab = 'Nr2f2 (log10 cpm)', ylab = 'Flag_Myc (log10 cpm)', main = 'Defining Recombined Cells\nCOUP-TFII-OE Mice Only')
#   abline(v = 1.6, col = 'red')
# dev.off()

sum(mat.exp2.cre[,'Nr2f2'] > 1.6)
sum(mat.exp2.cre[,'Nr2f2'] > 1.6) / length(cells.cre)

cells.recomb = as.character(cast.exp2.cre[Flag_Nr2f2_fusion + Flag_myc_transgene > 0, cell.name])

## Add to cell.info and save
# for now use detection of Flag-myc transgene
exp2.cell.info.dt[, cell.genotype := genotype]
exp2.cell.info.dt[cell.name %in% cells.recomb, cell.genotype := 'Flag.expressing']
exp2.cell.info.dt[, cell.genotype_color := gg_color_hue(3)[factor(cell.genotype)]]

save(exp2.cell.info.dt, file = 'data/fetal_venule_exp2.cell_info_dt.RData')


# all r1 (degraded, high-rRNA cells) ------
cordir='iRPCA/all_r1/'
system(paste0('mkdir ',cordir));
system(paste0('mkdir ',file.path(cordir,'biplots')));

dt.pca = copy(dt.exp2.good)
setkey(dt.pca)
cell.info = copy(exp2.cell.info.dt)
cell.info[, experiment := genotype]
cell.info[, experiment_color := genotype_color]

n.pcs = 15
pca.d1 <- dimplots.pca.lowMem(dt.pca,cell.info,cordir,suffix='',max.genes = 30,ncp=n.pcs,
                              min.cells.detect=1,n.cores = 8)
plot.ggpairs.wExptCols(cell.info,dir=cordir,suffix='',num.pcs = n.pcs,height=25,width=25,sigType = 'Sig',annot.expt = F)

registerDoMC(8)
foreach(i=1:n.pcs) %dopar% {
  make.facet.biplots(dt.pca,c(paste0('PC',i),paste0('PC',i)),cordir,ax.suf = c('.pos','.neg'),size=1.5,
                     height = 18,width = 22,num.genes = 30)
}

cast.data = dcast.data.table(dt.pca, cell.name ~ gene, value.var = 'log10.cpm')
setkey(cast.data)
pc.scores = fread(paste0(cordir, 'PC_allscores.csv'))
pc.scores = merge(pc.scores, cast.data, by='cell.name')
pc.scores = merge(pc.scores, cell.info, by='cell.name')
setkey(pc.scores)
# 11 cells with very low genes and no expression of Pecam1. Remove these (for now).
# PCA finds no enriched genes (although I would expect Rn45s to be enriched)
# They show up in several PCs as being in the "dropout quadrant". Would be great to show in 
# theory paper
# They turn out to have a very high fraction of reads going to rRNA - possibly very degraded cells
ggplot(pc.scores, aes(PC15.pos + PC15.neg, frac.rRNA)) + geom_point() + 
  geom_hline(yintercept = .65) + theme_bw()
ggsave('iRPCA/all_r1/PC15_fracRn45s_separation.pdf', height = 4, width = 4)
ggsave('qc/all_r1_PC15_fracRn45s_separation.pdf', height = 4, width = 4)

cells.high.rRNA = cell.info[frac.rRNA > .65, cell.name]
save(cells.high.rRNA, file = 'iRPCA/all_r1/cells_highRn45s.RData')

# all r2 ------
cordir='iRPCA/all_r2/'
system(paste0('mkdir ',cordir));
system(paste0('mkdir ',file.path(cordir,'biplots')));

load('iRPCA/all_r1/cells_highRn45s.RData')

dt.pca = dt.exp2.good[!cell.name %in% cells.high.rRNA]
setkey(dt.pca)
cell.info = exp2.cell.info.dt[!cell.name %in% cells.high.rRNA]
cell.info[, experiment := genotype]
cell.info[, experiment_color := genotype_color]

n.pcs = 15
pca.d1 <- dimplots.pca.lowMem(dt.pca,cell.info,cordir,suffix='',max.genes = 30,ncp=n.pcs,
                              min.cells.detect=1,n.cores = 8)
plot.ggpairs.wExptCols(cell.info,dir=cordir,suffix='',num.pcs = n.pcs,height=25,width=25,sigType = 'Sig',annot.expt = F)

registerDoMC(8)
foreach(i=1:n.pcs) %dopar% {
  make.facet.biplots(dt.pca,c(paste0('PC',i),paste0('PC',i)),cordir,ax.suf = c('.pos','.neg'),size=1.5,
                     height = 18,width = 22,num.genes = 30)
}

cast.data = dcast.data.table(dt.pca, cell.name ~ gene, value.var = 'log10.cpm')
setkey(cast.data)
pc.scores = fread(paste0(cordir, 'PC_allscores.csv'))
pc.scores = merge(pc.scores, cast.data, by='cell.name')
pc.scores = merge(pc.scores, cell.info, by='cell.name')
setkey(pc.scores)

require(plot3Drgl)
with(pc.scores, plot3d(PC1.pos, PC4.pos, PC3.pos, col = vecs2rgb(Ccnf, Ccne2, Mcm7), size = 5))

# all r2 no CC ------
cordir='iRPCA/all_r2_noCC/'
system(paste0('mkdir ',cordir));
system(paste0('mkdir ',file.path(cordir,'biplots')));

load('~/singlecell-pipeline/fetal_venule/exp1_hiseq/genes_cc.RData')
load('iRPCA/all_r1/cells_highRn45s.RData')

dt.pca = dt.exp2.good[!cell.name %in% cells.high.rRNA][!gene %in% genes.cc]
setkey(dt.pca)
cell.info = exp2.cell.info.dt[!cell.name %in% cells.high.rRNA]
cell.info[, experiment := genotype]
cell.info[, experiment_color := genotype_color]

n.pcs = 15
pca.d1 <- dimplots.pca.lowMem(dt.pca,cell.info,cordir,suffix='',max.genes = 30,ncp=n.pcs,
                              min.cells.detect=1,n.cores = 8)
plot.ggpairs.wExptCols(cell.info,dir=cordir,suffix='',num.pcs = n.pcs,height=25,width=25,sigType = 'Sig',annot.expt = F)

registerDoMC(8)
foreach(i=1:n.pcs) %dopar% {
  make.facet.biplots(dt.pca,c(paste0('PC',i),paste0('PC',i)),cordir,ax.suf = c('.pos','.neg'),size=1.5,
                     height = 18,width = 22,num.genes = 30)
}

make.facet.biplots(dt.pca,c(paste0('PC',2),paste0('PC',13)),cordir,size=1.5)

cast.data = dcast.data.table(dt.pca, cell.name ~ gene, value.var = 'log10.cpm')
setkey(cast.data)
pc.scores = fread(paste0(cordir, 'PC_allscores.csv'))
pc.scores = merge(pc.scores, cast.data, by='cell.name')
pc.scores = merge(pc.scores, cell.info, by='cell.name')
setkey(pc.scores)

ggplot(pc.scores, aes(PC2.pos, PC2.neg, color = Flag_myc_transgene)) + 
  geom_point()
ggplot(pc.scores, aes(PC2.score, Flag_myc_transgene + Flag_Nr2f2_fusion)) + 
  geom_point() + geom_smooth()

ggplot(pc.scores, aes(rank(PC2.score), Flag_Nr2f2_fusion)) + 
  geom_point() + geom_smooth()
ggplot(pc.scores, aes(rank(PC2.score), Gja4, group = genotype, color = genotype)) + 
  geom_point() + geom_smooth() + geom_vline(xintercept = 110)
ggplot(pc.scores, aes(rank(PC2.score), Nr2f2, group = genotype, color = genotype)) + 
  geom_point() + geom_smooth()
ggplot(pc.scores, aes(rank(PC2.score), Aplnr, group = genotype, color = genotype)) + 
  geom_point() + geom_smooth()
ggplot(pc.scores, aes(rank(PC2.score), Sox7, group = genotype, color = genotype)) + 
  geom_point() + geom_smooth()

ggplot(pc.scores, aes(PC2.score, PC13.score, group = genotype, color = genotype)) + 
  geom_point() 

smr.expt = unique(pc.scores[,.(mean(PC2.score), length(cell.name), sum(PC2.score < -.3)),
                            by = .(plate.name, genotype)])

# difference is significant when pooling all cells
fisher.test(table(pc.scores[, .(PC2.score < -.3, genotype)]))

# difference is (barely) not significant with experiments as unique samples
smr.expt[,frac.artl := V3/V2]
ggplot(smr.expt, aes(genotype, frac.artl, group = genotype, fill = genotype)) +
  geom_boxplot() + ylim(c(0,max(smr.expt[,frac.artl])))

t.test(formula = frac.artl ~ genotype, data = smr.expt)
kruskal.test(formula = frac.artl ~ genotype, data = smr.expt)

ggplot(pc.scores, aes(PC2.score, group = genotype, fill = genotype)) + 
  geom_density(position = 'identity', alpha = .5) 

ggplot(pc.scores, aes(Flag_Nr2f2_fusion, Nr2f2)) + 
  geom_point() + geom_smooth()


smr.FlagMyc = unique(pc.scores[,.(sum(Flag_Nr2f2_fusion > 0)/length(unique(cell.name))),
                            by = .(plate.name, genotype)])
ggplot(smr.FlagMyc, aes(genotype, V1, group = genotype, fill = genotype)) +
  geom_boxplot() + ylim(c(0,max(smr.FlagMyc[,V1]))) + ggtitle('Fraction of Cells Expressing Flag-Nr2f2 Fusion')
ggsave('qc/fractionCellsExpr_Flag-Nr2f2-Fusion.pdf', height = 4, width = 5)
