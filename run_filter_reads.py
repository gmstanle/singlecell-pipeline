
# coding: utf-8

# In[4]:
import sys
sys.path.append("/local10G/resources/py2.7.9_virtualenv/lib/python2.7/site-packages/")
input_bam = sys.argv[1]
output_prefix = sys.argv[2]
gtf_reference = sys.argv[3]

print input_bam
print output_prefix
print gtf_reference
# In[ ]:

import HTSeq as ht

# perform cut -d';' -f 1 <ref.gtf> | awk '($3 ~ /exon/)' | uniq
# before loading reference so that every (correctly ordered) exon appears only once
gtf_file=ht.GFF_Reader(gtf_reference)

# If the gtf file has genes with >1 name or overlapping genes, reads
# that map to those may not be counted
# Sequentially name non-contiguous exons for each gene
# feature.name defaults to gene_id

i=0;
MIN_EXON_SEP=1

exon_num={}
prev_end={}
exons=ht.GenomicArrayOfSets("auto",stranded=False)
for feature in gtf_file:
  if feature.type == "exon":
    exon_num[feature.name]=0
    prev_end[feature.name]=0

for feature in gtf_file:
  # initialize prev_name and end
  if feature.type == "exon":
    if (feature.iv.start - prev_end[feature.name]) > MIN_EXON_SEP:
      exon_num[ feature.name ]+=1
      
    exon_name = feature.name + "_e" + format(exon_num[feature.name],'02')
    prev_end[feature.name]=feature.iv.end
    exons[ feature.iv ] += exon_name


# In[5]:



#def filter_reads(input_bam, output_prefix):
# import HTSeq as ht
print "Filtering Reads"
all_reads=ht.BAM_Reader(input_bam)
good_reads_writer=ht.BAM_Writer.from_BAM_Reader(output_prefix+".good_reads.bam",all_reads)
good_jun_reads_writer=ht.BAM_Writer.from_BAM_Reader(output_prefix+".good_jun_reads.bam",all_reads)
intronic_reads_writer=ht.BAM_Writer.from_BAM_Reader(output_prefix+".intronic_reads.bam",all_reads)
mult_reads_writer=    ht.BAM_Writer.from_BAM_Reader(output_prefix+".mult_aligned_reads.bam",all_reads)
skipped_iv=ht.GenomicArray("auto",stranded=False,typecode="i")
c11=0; c12=0; c21=0; c10=0; c00=0; num_good_reads=0; num_bad_reads=0; nh2=0; num_jun_reads=0;

bad_cigars=set(['I','S','H'])
num_cycles=0

for read in all_reads:

  chunk=ht.GenomicArrayOfSets("auto",stranded=False)
  notstarted=True
  is_good_read=True
  is_jun_read=False
  has_intronic=False
  has_mult_algn=False
  # check that there is exactly 1 unique mapping
  # Warning: this depends on the 'NH' field existing
  opts=dict(read.optional_fields)
  if opts["NH"] != 1:
    is_good_read=False
    nh2+=1
  else:
    num_sections=0
    for a in read.cigar: # does this read have a skipped part?
      prev_gene=''
      num_sections+=1
      if a.type == "N":
        is_jun_read=True
        num_jun_reads+=1

    if is_jun_read:
      section_num=0
      chunkend=0; chunkstart=0
      for a in read.cigar:
        section_num+=1
        if a.type != "N" and a.type not in bad_cigars and section_num != num_sections:
          # get the position of the start of the chunk
          if notstarted:
            chunk_fs = set([])
            notstarted=False
          for iv, fs in exons[ a.ref_iv ].steps():
            # if a chunk extends past exon,
            # the read is bad
            if len(fs)==0:
              is_good_read=False
            else:
              chunk_fs |= fs
        elif a.type not in bad_cigars:  # if we are on the skipped part or end of alignment
          if a.type=="N":
            notstarted=True
          else:
            if notstarted:
              chunk_fs = set([])
              notstarted=False
            for iv, fs in exons[ a.ref_iv ].steps():
              # if a chunk extends past exon,
              # the read is bad
              if len(fs)==0:
                is_good_read=False
              else:
                chunk_fs |= fs
          # this is the vast majority of bad chunks -
          # align wholly to non-exonic part of genome
          if is_good_read and len(chunk_fs)==1:
            c11+=1

            # check that previous chunk was on same gene
            if (list(chunk_fs)[0][:-4] != prev_gene) and (prev_gene != ""):
              is_good_read=False
            else:
              prev_gene = list(chunk_fs)[0][:-4]
          elif is_good_read and len(chunk_fs) > 1:
            is_good_read=False
            # most of these are on parts of genome w/multiple 
            # gene_id annotations
            c12+=1
          elif is_good_read:
            is_good_read=False
            c00+=1
    else:# if there is no skipped part in read
      read_exons=set()
      read_genes=str()
      iv_seq = ( co.ref_iv for co in read.cigar if co.type == "M" and co.size > 0 )
      for iv in iv_seq:
        for iv2, fs2 in exons[ iv ].steps():
          # check if read has any alignment to intronic (genomic) seq
          if not len(fs2): 
            is_good_read=False
            has_intronic=True
          else:
            read_exons |= fs2
            read_genes += ","+list(fs2)[0][:-4]
    
      # test if read aligns to multiple genes
      read_genes=set(read_genes.split(","))
      if len(read_genes) > 2 and is_good_read:
        is_good_read=False
        has_mult_align=True
  if is_good_read: # write good reads and add count to exons
    good_reads_writer.write(read)
    num_good_reads+=1
    if is_jun_read:
      good_jun_reads_writer.write(read)
  elif has_intronic:
    intronic_reads_writer.write(read)
  elif has_mult_algn:
    mult_reads_writer.write(read)

